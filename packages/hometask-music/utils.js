function createElement({ tag, attributes, parent, content }) {
    const el = document.createElement(tag);
  
    if (attributes) {
      for (const attr in attributes) {
        el.setAttribute(attr, attributes[attr]);
      }
    }
  
    if (content) {
      el.textContent = content;
    }
  
    if (parent) {
      parent.appendChild(el);
    }
  
    return el;
  }
  
  export { createElement };
  
