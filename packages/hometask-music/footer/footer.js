import "./footer.css";
import { createElement } from "../utils.js";

export default function setUpFooter(parent) {
    createElement({
      tag: "footer",
      attributes: {
        class: "footer",
      },
      parent,
    });
  
    const footer = document.querySelector(".footer");

    createElement({
        tag: "p",
        attributes: {
          class: "footer-nav__item",
        },
        parent: footer,
        content: "About Us",
      });
    
      createElement({
        tag: "p",
        attributes: {
          class: "footer-nav__item",
        },
        parent: footer,
        content: "Contact",
      });
    
      createElement({
        tag: "p",
        attributes: {
          class: "footer-nav__item",
        },
        parent: footer,
        content: "CR Info",
      });
    
      createElement({
        tag: "p",
        attributes: {
          class: "footer-nav__item",
        },
        parent: footer,
        content: "Twitter",
      });
    
      createElement({
        tag: "p",
        attributes: {
          class: "footer-nav__item",
        },
        parent: footer,
        content: "Facebook",
      });
    }
