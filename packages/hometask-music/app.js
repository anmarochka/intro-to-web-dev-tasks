import { createElement } from "./utils"; 
import setUplandingPageHeader from "./header/landingPageHeader";
import setUplandingPageMain from "./main/landingPageMain";
import setUpFooter from  "./footer/footer";
import setUpFeaturePageHeader from "./header/featureHeader";
import setUpSignUpPageHeader from "./header/signUpHeader";
import setUpfeaturePageMain from "./main/featurePageMain";
import setUpSignUpPageMain from "./main/signUpMain";

const app = document.querySelector("#app");

createElement({tag: 'header', parent: app});
const newHeader = document.querySelector('header');

createElement({tag: 'main', parent: app});
const newMain = document.querySelector('main');
setUplandingPageHeader(newHeader);
setUplandingPageMain(newMain);
setUpFooter(app);

const { body } = document;

function handleClicklistener(event) {
    if (event.target.id === 'buttondiscover') {
        document.querySelector(".app__header").remove();
        document.querySelector(".app__main").remove();
        document.querySelector(".main__image").remove();
        setUpFeaturePageHeader(newHeader);
        setUpfeaturePageMain(newMain);
    }

    if (event.target.id === 'buttonjoin' || event.target.id === 'button-JoinNow' || event.target.id === 'join') {
        document.querySelector(".app__header").remove();
        document.querySelector(".app__main").remove();
        document.querySelector(".main__image").remove();
        setUpSignUpPageHeader(newHeader);
        setUpSignUpPageMain(newMain);
    }

    if (event.target.id === 'discover') {
        document.querySelector(".app__header").remove();
        document.querySelector(".app__main").remove();
        document.querySelector(".main__image").remove();
        setUplandingPageHeader(newHeader);
        setUplandingPageMain(newMain);
    }
}

body.addEventListener('click', handleClicklistener);
newMain.addEventListener('click', handleClicklistener);
