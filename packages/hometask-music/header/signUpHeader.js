import { createElement } from "../utils.js";
import logo from "..//assets/icons/logo.svg";

export default function setUpSignUpPageHeader(parent) {
    createElement({
      tag: "div",
      attributes: {
        class: "app__header",
      },
      parent,
    });
  

    const header = document.querySelector(".app__header");

    createElement(
        {tag: 'img', 
        attributes: {class: 'header__logo', src: logo}, 
        parent: header,
    }); 

    createElement({
        tag: "a",
        attributes: {
          class: "header__link",
        },
        parent: header,
        content: "Simo",
      });
    
      createElement({
        tag: "a",
        attributes: {
          class: "header__link",
          id: "discover",
        },
        parent: header,
        content: "Discover",
      });
    
      createElement({
        tag: "a",
        attributes: {
          class: "header__link",
        },
        parent: header,
        content: "Sign In",
      });
    }
