import { createElement } from "../utils.js";
import logo from "../assets/icons/logo.svg";

export default function setUpFeaturePageHeader(parent) {
    createElement({
      tag: "div",
      attributes: {
        class: "app__header",
      },
      parent,
    });
  

    const header = document.querySelector(".app__header");

    createElement(
        {tag: 'img', 
        attributes: {class: 'header__logo', src: logo}, 
        parent: header,
    }); 

    createElement({
        tag: "a",
        attributes: {
          class: "header__link",
        },
        parent: header,
        content: "Simo",
      });
    
      createElement({
        tag: "a",
        attributes: {
          class: "header__link",
          id: "join",
        },
        parent: header,
        content: "Join",
      });
    
      createElement({
        tag: "a",
        attributes: {
          class: "header__link",
        },
        parent: header,
        content: "Sign In",
      });
    }
