import "./featurePageMain.css";
import { createElement } from "../utils.js";
import view from '../assets/images/music-titles.png';

export default function setUpfeaturePageMain(parent) {
    createElement({
      tag: "div",
      attributes: {
        class: "app__main app__main-discovery",
      },
      parent,
    });

    // eslint-disable-next-line no-undef
    const main = document.querySelector(".app__main");

    createElement({
        tag: "h1",
        attributes: {
          class: "main__title-feat",
        },
        parent: main,
        content: 'Discover new music'
      });

    createElement({
        tag: 'div', 
        attributes: {
          class: 'main__buttons-featPage'
        }, 
        parent: main
      });

      const buttonsFeat = document.querySelector('.main__buttons-featPage');
  
    createElement({
        tag: 'button', 
        attributes: {
          class: 'main__button-feat'
        }, 
        parent: buttonsFeat, 
        content: 'Charts'
      });
  
    createElement({
        tag: 'button', 
        attributes: {
          class: 'main__button-feat'
        }, 
        parent: buttonsFeat, 
        content: 'Songs'
      });
  
    createElement({
        tag: 'button', 
        attributes: {
          class: 'main__button-feat'
        }, 
        parent: buttonsFeat, 
        content: 'Artists'
      });

    createElement({
        tag: "img",
        attributes: {
          class: "main__image-feat",
          src: view,
        },
        parent
      });

    createElement({
        tag: "p",
        attributes: {
          class: "main__text-feat",
        },
        parent: main,
        content: 'By joing you can benefit by listening to the latest albums released'
      });
}
