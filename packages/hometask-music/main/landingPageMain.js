import "./landingPageMain.css";
import { createElement } from "../utils.js";
import backgroundGirl from '../assets/images/background-page-landing.png';

export default function setUplandingPageMain(parent) {
    createElement({
      tag: "div",
      attributes: {
        class: "app__main",
      },
      parent,
    });

    const main = document.querySelector(".app__main");

    createElement({
        tag: "h1",
        attributes: {
          class: "main__title",
        },
        parent: main,
        content: 'Feel the music'
      });

      createElement({
        tag: "p",
        attributes: {
          class: "main__text",
        },
        parent: main,
        content: 'Stream over 10 million songs with one click'
      });


      createElement({
        tag: "button",
        attributes: {
          class: "main__button",
          id: 'button-JoinNow'
        },
        parent: main,
        content: 'Join now'
      });


      createElement({
        tag: "img",
        attributes: {
          class: "main__image",
          src: backgroundGirl,
        },
        parent
      });
}
