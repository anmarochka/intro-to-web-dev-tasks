import "./signUpMain.css";
import { createElement } from "../../../../utils/create_element_function";
import signUpGirl from "../../../../assets/images/background-page-sign-up.png";

export default function setUpMainJoinPage(parent) {
    createElement({
      tag: "div",
      attributes: {
        class: "app__main app__main-join",
      },
      parent,
    });
  
    const main = document.querySelector(".app__main");
  
    createElement({
      tag: "div",
      attributes: {
        class: "main__form",
      },
      parent: main,
    });
  
    const mainForm = document.querySelector(".main__form");
  
    createElement({
      tag: "div",
      attributes: {
        class: "form__item form__item-name",
      },
      parent: mainForm,
    });
  
    const formName = document.querySelector(".form__item-name");
  
    createElement({
      tag: "p",
      attributes: {
        class: "form__item__info",
      },
      parent: formName,
      content: "Name:",
    });
  
    createElement({
      tag: "input",
      attributes: {
        class: "form__item__input",
        type: "text",
      },
      parent: formName,
    });
  
    createElement({
      tag: "div",
      attributes: {
        class: "form__item form__item-password",
      },
      parent: mainForm,
    });
  
    const formPassword = document.querySelector(".form__item-password");
  
    createElement({
      tag: "p",
      attributes: {
        class: "form__item__info",
      },
      parent: formPassword,
      content: "Password:",
    });
  
    createElement({
      tag: "input",
      attributes: {
        class: "form__item__input",
        type: "text",
      },
      parent: formPassword,
    });
  
    createElement({
      tag: "div",
      attributes: {
        class: "form__item form__item-email",
      },
      parent: mainForm,
    });
  
    const formEmail = document.querySelector(".form__item-email");
  
    createElement({
      tag: "p",
      attributes: {
        class: "form__item__info",
      },
      parent: formEmail,
      content: "e-mail:",
    });
  
    createElement({
      tag: "input",
      attributes: {
        class: "form__item__input",
        type: "text",
      },
      parent: formEmail,
    });

  createElement({
    tag: "button",
    attributes: {
      class: "main__button",
      id: "button-JoinNow",
    },
    parent: main,
    content: "Join now",
  });

  createElement({
    tag: "img",
    attributes: {
      class: "main__image",
      src: signUpGirl,
    },
    parent,
  });
}
